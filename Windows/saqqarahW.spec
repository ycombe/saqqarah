# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

from saqqarah import version 

added_files = [
               ('C:\\windows\\system32\\msvcp140_1.dll', '.'),
               ('C:\\windows\\system32\\vcruntime140_1.dll', '.'),
               ('c:\python37\lib\site-packages\\saqqarah\\ui\\', 'saqqarah\\ui'),
               ('c:\python37\lib\site-packages\\saqqarah\\fonts\\', 'saqqarah\\fonts')
              ]

a = Analysis(['saqqarahW.py'],
             pathex=['/home/yves/Docker/src'],
             binaries=[],
             datas=added_files,
             hiddenimports=['PySide2.QtXml'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='saqqarahW-' + version() ,
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
